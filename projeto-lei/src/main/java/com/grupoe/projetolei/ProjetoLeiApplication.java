package com.grupoe.projetolei;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoLeiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoLeiApplication.class, args);
	}

}
