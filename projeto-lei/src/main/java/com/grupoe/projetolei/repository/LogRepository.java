package com.grupoe.projetolei.repository;
import com.grupoe.projetolei.entities.Bills;
import com.grupoe.projetolei.entities.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepository extends JpaRepository<Log, Long> {

}
