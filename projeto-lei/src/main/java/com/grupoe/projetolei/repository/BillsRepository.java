package com.grupoe.projetolei.repository;
import com.grupoe.projetolei.entities.Author;
import com.grupoe.projetolei.entities.Bills;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BillsRepository extends JpaRepository<Bills, String> {
}
