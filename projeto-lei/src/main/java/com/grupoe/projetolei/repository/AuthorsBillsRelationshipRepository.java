package com.grupoe.projetolei.repository;
import com.grupoe.projetolei.entities.Author;
import com.grupoe.projetolei.entities.AuthorsBillsRelationship;
import com.grupoe.projetolei.entities.AuthorsBillsRelationshipId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AuthorsBillsRelationshipRepository extends JpaRepository<AuthorsBillsRelationship, AuthorsBillsRelationshipId> {
    Optional<List<String>> findAllByIdAutor(Long idAuthor);
}
