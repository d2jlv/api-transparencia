package com.grupoe.projetolei.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "projeto_lei_x_autor")
@IdClass(AuthorsBillsRelationshipId.class)
public class AuthorsBillsRelationship {
    @Id
    private String numeroProjetoLei;
    @Id
    private Long idAutor;

    public AuthorsBillsRelationship(String numeroProjetoLei, Long idAutor) {
        this.numeroProjetoLei = numeroProjetoLei;
        this.idAutor = idAutor;
    }

    public AuthorsBillsRelationship() {
    }

    public String getNumeroProjetoLei() {
        return numeroProjetoLei;
    }

    public void setNumeroProjetoLei(String numeroProjetoLei) {
        this.numeroProjetoLei = numeroProjetoLei;
    }

    public Long getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(Long idAutor) {
        this.idAutor = idAutor;
    }
}
