package com.grupoe.projetolei.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "projeto_lei")
public class Bills {
    @Id
    private String numeroProjeto;
    private String assunto;
    @Column(name = "anotacao")
    private  String status;
    private Integer ano;
    private  String link;

    public Bills(String numeroProjeto, String assunto, String status, Integer ano, String link) {
        this.numeroProjeto = numeroProjeto;
        this.assunto = assunto;
        this.status = status;
        this.ano = ano;
        this.link = link;
    }

    public Bills() {
    }

    public String getNumeroProjeto() {
        return numeroProjeto;
    }

    public void setNumeroProjeto(String numeroProjeto) {
        this.numeroProjeto = numeroProjeto;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
