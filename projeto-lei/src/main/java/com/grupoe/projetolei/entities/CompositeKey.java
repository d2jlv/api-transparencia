package com.grupoe.projetolei.entities;

import java.io.Serializable;
import java.util.Objects;

public class CompositeKey implements Serializable {
    private String numeroProjetoLei;
    private Long idAutor;

    public CompositeKey() {
    }

    public CompositeKey(String numeroProjetoLei, Long idAutor) {
        this.numeroProjetoLei = numeroProjetoLei;
        this.idAutor = idAutor;
    }

    public String getNumeroProjetoLei() {
        return numeroProjetoLei;
    }

    public void setNumeroProjetoLei(String numeroProjetoLei) {
        this.numeroProjetoLei = numeroProjetoLei;
    }

    public Long getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(Long idAutor) {
        this.idAutor = idAutor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CompositeKey)) return false;
        CompositeKey that = (CompositeKey) o;
        return Objects.equals(getNumeroProjetoLei(), that.getNumeroProjetoLei()) &&
                Objects.equals(getIdAutor(), that.getIdAutor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNumeroProjetoLei(), getIdAutor());
    }
}
