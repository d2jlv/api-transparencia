package com.grupoe.projetolei.entities;

import javax.persistence.*;

@Entity
@Table(name="autor")
public class Author {
    @Id
    private Long id;

    private String idSiteProjeto;
    private String nome;
    private String apelido;
    private String partido;
    private String atividade;
    private String formacao;
//    @Column(name = "datanascimento")
    private String dataNascimento;
//    @Column(name = "estadocivil")
    private String estadoCivil;
//    @Column(name = "votosrecebidos")
    private Integer votosRecebidos;
    private String legislatura;

    public Author(Long id, String idSiteProjeto, String nome, String apelido, String partido, String atividade, String formacao, String dataNascimento, String estadoCivil, Integer votosRecebidos, String legislatura) {
        this.id = id;
        this.idSiteProjeto = idSiteProjeto;
        this.nome = nome;
        this.apelido = apelido;
        this.partido = partido;
        this.atividade = atividade;
        this.formacao = formacao;
        this.dataNascimento = dataNascimento;
        this.estadoCivil = estadoCivil;
        this.votosRecebidos = votosRecebidos;
        this.legislatura = legislatura;
    }

    public Author() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdSiteProjeto() {
        return idSiteProjeto;
    }

    public void setIdSiteProjeto(String idSiteProjeto) {
        this.idSiteProjeto = idSiteProjeto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getPartido() {
        return partido;
    }

    public void setPartido(String partido) {
        this.partido = partido;
    }

    public String getAtividade() {
        return atividade;
    }

    public void setAtividade(String atividade) {
        this.atividade = atividade;
    }

    public String getFormacao() {
        return formacao;
    }

    public void setFormacao(String formacao) {
        this.formacao = formacao;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Integer getVotosRecebidos() {
        return votosRecebidos;
    }

    public void setVotosRecebidos(Integer votosRecebidos) {
        this.votosRecebidos = votosRecebidos;
    }

    public String getLegislatura() {
        return legislatura;
    }

    public void setLegislatura(String legislatura) {
        this.legislatura = legislatura;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", idSiteProjeto='" + idSiteProjeto + '\'' +
                ", nome='" + nome + '\'' +
                ", apelido='" + apelido + '\'' +
                ", partido='" + partido + '\'' +
                ", atividade='" + atividade + '\'' +
                ", formacao='" + formacao + '\'' +
                ", dataNascimento='" + dataNascimento + '\'' +
                ", estadoCivil='" + estadoCivil + '\'' +
                ", votosRecebidos=" + votosRecebidos +
                ", legislatura='" + legislatura + '\'' +
                '}';
    }
}
