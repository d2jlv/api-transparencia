package com.grupoe.projetolei.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "log_projeto")
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "dataAtualizacao")
    private LocalDateTime updateDateTime;
    @Column(name = "origem")
    private String origin;
    private String output;

    public Log(LocalDateTime updateDateTime, String origin, String output) {
        this.updateDateTime = updateDateTime;
        this.origin = origin;
        this.output = output;
    }

    public Log() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
}
