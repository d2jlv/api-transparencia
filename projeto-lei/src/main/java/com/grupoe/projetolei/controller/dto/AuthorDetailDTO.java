package com.grupoe.projetolei.controller.dto;

import com.grupoe.projetolei.entities.Author;
import com.grupoe.projetolei.entities.Bills;

import java.util.List;

public class AuthorDetailDTO {
    private Author author;
    private List<Bills> projetos;

    public AuthorDetailDTO(Author author, List<Bills> projetos) {
        this.author = author;
        this.projetos = projetos;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Bills> getProjetos() {
        return projetos;
    }

    public void setProjetos(List<Bills> projetos) {
        this.projetos = projetos;
    }
}
