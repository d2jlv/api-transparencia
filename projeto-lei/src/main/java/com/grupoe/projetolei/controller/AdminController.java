package com.grupoe.projetolei.controller;

import com.grupoe.projetolei.usecase.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AuthorService service;

//    public AdminController(AuthorService service) {
//        this.service = new AuthorService();
//    }

    @GetMapping("/atualizar/projetos")
    public String update(){
        return service.update();
    }
}
