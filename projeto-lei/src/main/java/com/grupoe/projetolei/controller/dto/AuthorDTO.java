package com.grupoe.projetolei.controller.dto;

public class AuthorDTO {
    private Long id;
    private String apelido;

    public AuthorDTO(Long id, String apelido) {
        this.id = id;
        this.apelido = apelido;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    @Override
    public String toString() {
        return "AuthorDTO{" +
                "id=" + id +
                ", apelido='" + apelido + '\'' +
                '}';
    }
}
