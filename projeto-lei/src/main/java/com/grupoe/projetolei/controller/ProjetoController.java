package com.grupoe.projetolei.controller;

import com.grupoe.projetolei.controller.dto.AuthorDTO;
import com.grupoe.projetolei.controller.dto.AuthorDetailDTO;
import com.grupoe.projetolei.entities.Author;
import com.grupoe.projetolei.entities.Bills;
import com.grupoe.projetolei.repository.AuthorRepository;
import com.grupoe.projetolei.repository.AuthorsBillsRelationshipRepository;
import com.grupoe.projetolei.repository.BillsRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/projeto")
public class ProjetoController {

    private AuthorRepository authorRepo;
    private AuthorsBillsRelationshipRepository relationshipRepo;
    private BillsRepository billsRepo;

    public ProjetoController(AuthorRepository authorRepo, AuthorsBillsRelationshipRepository relationshipRepo, BillsRepository billsRepo) {
        this.authorRepo = authorRepo;
        this.relationshipRepo = relationshipRepo;
        this.billsRepo = billsRepo;
    }

    @GetMapping("/autores")
    public ResponseEntity<List<AuthorDTO>> getAuthors() {
        List<AuthorDTO> authorsDTO = new ArrayList<>();
        
        try{
           List<Author> authors = authorRepo.findAll();
           authors.forEach(author -> authorsDTO.add(new AuthorDTO(author.getId(),author.getApelido())));
           return ResponseEntity.ok(authorsDTO);
           
       }catch (Exception e){
           return ResponseEntity.unprocessableEntity().build();
       }
    }

    @GetMapping("autores/{id}")
    public ResponseEntity<AuthorDetailDTO> getAuthorDetail(@PathVariable(value = "id") long authorId){
        Optional<Author> author = authorRepo.findById(authorId);
        System.out.println("hey   " + author);
        if(!author.isPresent()){
            return ResponseEntity.noContent().build();
        }

        Optional<List<String>> billsId  = relationshipRepo.findAllByIdAutor(authorId);
        List<Bills> billsList = new ArrayList<>();
        billsId.ifPresent((strings) -> {
            strings.forEach((id) -> {
                Optional<Bills> bill = billsRepo.findById(id);
                bill.ifPresent((b) -> billsList.add(b));
            });
        });

        return ResponseEntity.ok(new AuthorDetailDTO(author.get(),billsList));
    }

}
