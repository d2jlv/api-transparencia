package com.grupoe.projetolei.usecase;

import com.grupoe.projetolei.entities.Log;
import com.grupoe.projetolei.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

@Service
public class AuthorService {
    @Autowired
    private LogRepository repository;
    private String ORIGIN = "admin-api";

//    @Autowired
//    public AuthorService(LogRepository repository) {
//        this.repository = repository;
//    }

    public String update(){
        Process exec;
        String commandLine = "node \"C:\\Users\\Juliane\\Documents\\FATEC\\4o SEMESTRE\\Engenharia Software 2\\web-crawler\\index.js\"";
        try {
            exec = Runtime.getRuntime().exec(commandLine);
            if ( exec.waitFor() == 0) {
                System.out.println("Executado.");
                StringBuilder output = new StringBuilder();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(exec.getInputStream()));

                String line;
                while ((line = reader.readLine()) != null) {
                    output.append(line + "\n");
                }
                this.logUpdate(output);
                return "Atualização realizada com sucesso";
            }else {
                return "Ocorreu um erro na atualização. \nEntre em contato com o suporte";
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "Finalizado!";
    }

    private void logUpdate(StringBuilder output) {
        String outputFormated = output.toString();

        Log log = new Log(LocalDateTime.now(), ORIGIN, outputFormated);
        repository.save(log);
        System.out.println("-------" + log.toString());
    }
}
