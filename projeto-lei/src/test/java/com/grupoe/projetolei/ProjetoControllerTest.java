package com.grupoe.projetolei;

import com.grupoe.projetolei.controller.ProjetoController;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class ProjetoControllerTest extends ProjetoLeiApplicationTests {

    private MockMvc mock;

    @Autowired
    private ProjetoController controller;

    @Before
    public  void setUp(){
        this.mock = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void getAuthorsTest() throws Exception{
        this.mock.perform(MockMvcRequestBuilders.get("/autores")).andExpect(MockMvcResultMatchers.status().isOk());
    }

}
