package com.grupoe.projetolei;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjetoLeiApplication.class)
@TestPropertySource(locations="classpath:test.properties")
class ProjetoLeiApplicationTests {

	@Test
	void contextLoads() {
	}

}
